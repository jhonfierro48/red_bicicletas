var Bicicleta = require('../../models/bicicleta');


//Resetear el arreglo para que quede vacío

beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });

});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [4.7127434,-74.1161973]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});


describe ('Bicicleta find id', () => {
    it ('Debe devolver la bici ocn id 1', () => {

        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'rojo', 'urbana', [4.7127434,-74.1161973]);
        var aBici2 = new Bicicleta(2, 'verde', 'montaña', [4.7127434,-74.1161973]);
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    });

});